@extends('layouts.app')

@section('content')

<body>
    <div class="container-fluid border h-100 w-100">
        <div class="row">
            <h1 class="mt-5 ml-5">My tickets</h1>
        </div>
        @if (\Session::has('Success'))
        <div class="alert alert-success">
            <ul>
                <li>{!! \Session::get('Success') !!}</li>
            </ul>
        </div>
        @endif
        <div class="row border w-100 mt-3">

            <div class="col-12">
                <h5 class="mb-3 mt-3">Ticket list:</h5>
                <table class="table table-striped table-responsive">
                    <tr>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Priority</th>
                        <th>Status</th>
                        <th>User</th>
                        <th>Project</th>
                        <th>Type</th>
                    </tr>
                    @forelse($tickets as $ticket)
                    @if($user->id == $ticket->user_id)
                    <tr>
                        <td>{{$ticket->title}}</td>
                        <td>{{$ticket->description}}</td>
                        <td>{{$ticket->priority}}</td>
                        <td>{{$ticket->status}}</td>
                        <td>{{$ticket->user_id}}</td>
                        <td>{{$ticket->project_id}}</td>
                        <td>{{$ticket->type}}</td>
                    </tr>
                    @endif
                    @empty
                    <tr>
                        <td colspan="3">You have no tickets</td>
                    </tr>
                    @endforelse
                </table>

            </div>
        </div>
    </div>
</body>
@endsection