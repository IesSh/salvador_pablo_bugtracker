@extends('layouts.app')

@section('content')

<body>
    <div class="container-fluid border h-100 w-100">
        <div class="row">
            <h1 class="mt-5 ml-5">Manage user roles</h1>
        </div>
        @if (\Session::has('Success'))
            <div class="alert alert-success">
                <ul>
                    <li>{!! \Session::get('Success') !!}</li>
                </ul>
            </div>
            @endif
        <div class="row border w-100 mt-3">
            <div class="col-4 mt-3">
                <!-- CONSEGUIR PASAR EL ID DEL USUARIO QUE SELECCIONE O NO FUNCIONARA -->
                <form action="updateRoles" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">

                    <h5>Select a user</h5>
                    <select multiple class="form-control" name="id">
                        <label for="id" value="{{$user->id}}"></label>
                        @foreach($users as $user)
                        <option value="{{$user->id}}" name="id">{{$user->name}}</option>
                        @endforeach
                    </select>


                    <label for="role_id" class="mt-5">Select a new role for the user:</label>
                    <div class="form-group bmd-form-group">
                        <label for="role_id">Role</label>
                        <select name="role_id" class="custom-select">
                            <option selected value="{{old('role_id') ? old('role_id') : $user->role_id}}">{{ $user->role->name }}</option>
                            @foreach($roles as $role)
                            @if ($role->name != $user->role->name)
                            <option value="{{old('id') ? old('id') : $role->id}}" name="role_id">{{$role->name}}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>

                    <input class="btn btn-primary mt-5" type="submit" value="Assign new role">
                </form>

                @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    <li>{{$error}}</li>
                </div>
                @endforeach

            </div>

            <div class="col-8">
                <h5 class="mb-3 mt-3">User list:</h5>
                <table class="table table-striped table-responsive">
                    <tr>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Rol</th>
                        <th>Email</th>
                    </tr>
                    @forelse($users as $user)
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->surname}}</td>
                        <td>{{$user->role->name}}</td>
                        <td>{{$user->email}}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3">There's no users</td>
                    </tr>
                    @endforelse
                </table>
                {{$users->links("pagination::bootstrap-4")}}
            </div>
        </div>
    </div>
</body>

</html>
@endsection