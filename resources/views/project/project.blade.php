@extends('layouts.app')

@section('content')

<body>
    <div class="container-fluid border h-100 w-100">
        <div class="row">
            <h1 class="mt-5 ml-5">My projects</h1>
        </div>
        @if (\Session::has('Success'))
        <div class="alert alert-success">
            <ul>
                <li>{!! \Session::get('Success') !!}</li>
            </ul>
        </div>
        @endif
        <div class="row border w-100 mt-3">

            <div class="col-12">
                <h5 class="mb-3 mt-3">Project list:</h5>
                <table class="table table-striped table-responsive">
                    <tr>
                        <th>Project's name</th>
                        <th>Description</th>
                        <th>Date of creation</th>
                        <th>Participants</th>
                    </tr>
                    @forelse($user->project as $project)
                    <tr>
                        <td>{{$project->project_name}}</td>
                        <td>{{$project->description}}</td>
                        <td>{{$project->created_at}}</td>
                        <td>
                            @forelse($project->users as $user)
                                <li>{{$user->name}}</li>
                            @empty
                            <td colspan="3">There's no users</td>
                            @endforelse
                        </td>
                        <td><a class="btn btn-primary btn-sm" href="/projects/{{$project->id}}">Details</a></td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3">You have no projects assigned</td>
                    </tr>
                    @endforelse
                </table>

            </div>
        </div>
    </div>
</body>
@endsection