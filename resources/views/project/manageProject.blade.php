@extends('layouts.app')

@section('content')

<body>
    <div class="container-fluid border h-100 w-100">
        <div class="row">
            <h1 class="mt-5 ml-5">Manage user roles</h1>
        </div>
        @if (\Session::has('Success'))
        <div class="alert alert-success">
            <ul>
                <li>{!! \Session::get('Success') !!}</li>
            </ul>
        </div>
        @endif
        @if (\Session::has('Encontrado'))
        <div class="alert alert-success">
            <ul>
                <li>{!! \Session::get('Encontrado') !!}</li>
            </ul>
        </div>
        @endif
        <div class="row border w-100 mt-3">
            <div class="col-4 mt-3">
                <!-- CONSEGUIR PASAR EL ID DEL USUARIO QUE SELECCIONE O NO FUNCIONARA -->
                <form action="updateUserProjects" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">

                    <h5>Select a user</h5>
                    <select multiple class="form-control" name="id">
                        <label for="id" value="{{$user->id}}"></label>
                        @foreach($users as $user)
                        <option value="{{$user->id}}" name="id">{{$user->name}}</option>
                        @endforeach
                    </select>


                    <label for="project_id" class="mt-5">Select a project to assign:</label>
                    <div class="form-group bmd-form-group">
                        <label for="project_id">Projects</label>
                        <select name="project_id" class="custom-select">
                            @foreach($projects as $project)
                            <option value="{{old('id') ? old('id') : $project->id}}" name="project_id">{{$project->project_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <input class="btn btn-primary mt-5" type="submit" value="Assign user to new project">
                </form>

                @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    <li>{{$error}}</li>
                </div>
                @endforeach

            </div>

            <div class="col-8">
                <h5 class="mb-3 mt-3">Project list:</h5>
                <table class="table table-striped table-responsive">
                    <tr>
                        <th>Project's name</th>
                        <th>Description</th>
                        <th>Date of creation</th>
                        <th>Participants</th>
                    </tr>
                    @forelse($projects as $project)
                    <tr>
                        <td>{{$project->project_name}}</td>
                        <td>{{$project->description}}</td>
                        <td>{{$project->created_at}}</td>
                        <td>
                            @forelse($project->users as $user)
                            <li>{{$user->name}}</li>
                            @empty
                            There's no users
                        @endforelse
                        </td>
                        <td><a class="btn btn-primary btn-sm" href="/projects/{{$project->id}}">Details</a></td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="3">You have no projects assigned</td>
                    </tr>
                    @endforelse
                </table>

            </div>
        </div>
    </div>
</body>

</html>
@endsection