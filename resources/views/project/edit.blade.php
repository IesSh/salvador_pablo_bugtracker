@extends('layouts.app')

@section('content')

<body>
    <div class="container-fluid border h-100 w-100">
        <div class="row">
            <h1 class="mt-5 ml-5">Edit project {{$project->id}}</h1>
        </div>
        <div class="row">
            <form action="/projects/{{$project->id}}" method="post">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <div>
                    <label for="name">Name</label>
                    <input type="text" name="name" value="{{ old('name') ? old('name') : $project->name }}">
                </div>

                <div>
                    <label for="description">Description</label>
                    <input type="text" name="description" value="{{ old('description') ? old('description') : $project->description }}">
                </div>

            </form>
        </div>
    </div>


</body>
@endsection