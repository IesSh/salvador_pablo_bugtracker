<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //con eloquent mejor, crear los timestap
        // Role::create([
        //     'name' => 'desarrollador'
        // ]);
        // Role::create([
        //     'name' => 'jefe'
        // ]);
        // Role::create([
        //     'name' => 'administrador'
        // ]);
        //query builder (DB)
        DB::table('roles')->insert([
            'id' => '1',
            'name' => 'Desarrollador'
        ]);

        DB::table('roles')->insert([
            'id' => '2',
            'name' => 'Jefe de proyecto'
        ]);

        DB::table('roles')->insert([
            'id' => '3',
            'name' => 'Administrador'
        ]);     
    }
}
