<?php

use App\Http\Controllers\ProjectController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\UserController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TicketController;
use ArielMejiaDev\LarapexCharts\Facades\LarapexChart;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('users', UserController::class); //->middleware('auth');
Route::resource('projects', ProjectController::class); //->middleware('auth');
Route::resource('tickets', TicketController::class); //->middleware('auth');
Route::put('updateRoles', [UserController::class, 'updateRoles']);    
Route::put('updateUserProjects', [ProjectController::class, 'updateUserProjects']);    

Route::get('/', function () {
    return view('welcome');
})->middleware('auth'); //redirigir al login cuando se trate de acceder a la página

//Route::get('/users', [UserController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/myProjects', [App\Http\Controllers\UserController::class, 'myProjects'])->name('myProjects');
Route::get('/manageUsers', [App\Http\Controllers\ProjectController::class, 'manageUsers'])->name('manageUsers');
Route::get('/manageProjects', [App\Http\Controllers\ProjectController::class, 'manageProjects'])->name('manageProjects');
Route::get('/myTickets', [App\Http\Controllers\TicketController::class, 'myTickets'])->name('myTickets');
//Route::get('roles', App\Http\Controllers\RoleController::class, 'index');
//rutas para charts
// Route::get('ticketsPriority', function() {
//     $chart = (new LarapexChart)->setTitle('ticketsPriority')
//     ->setXAxis(['Tickets', 'Priority'])
//     ->setDataset([100,50]);
//     return view('home', compact('chart'));
// });

// redirigir al login al hacer logout
Route::get('logout', [App\Http\Controllers\Auth\LoginController::class, 'logout']);

//Route::get('project', [App\Http\Controllers\Auth\LoginController::class, 'logout']);
