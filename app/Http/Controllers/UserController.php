<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Role;
use Illuminate\Support\Facades\Redirect;
use App\Models\Project;

class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = Auth::user();

        // dd($user);
        // $id = Auth::id();
        // dd($id);
        // $users = User::all();
        $name = $request->name;
        $role = $request->role;

        $roles = Role::all();

        $query = User::query();

        $path = "";
        if (!empty($name)) {
            $query = $query->where('name', 'like', "%$name%");
            $path = "name=$name";
        }
        if (!empty($role)) {
            $query = $query->where('role_id', $role);
            $path .= "&role=$role";
        }
        $users = $query->paginate(15);
        $users->withPath($request->path .
            '?name=' . $request->name
            . '&role=' . $request->role);

        // $users = $query->get();
        return view('role.role', [
            'users' => $users,
            'name' => $name,
            'role' => $role,
            'user' => $user,
            'roles' => $roles
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required',
            'role_id' => 'required'
        ]);

        $user = User::create($request->all());

        return redirect('/users');
    }

    public function myProjects()
    {
        $user = Auth::user();
        $id = $user->id;
        //many to many invertido
        $projects = Project::find($id);
        
        // echo($projects->users);
        // die();
        return view('project.project', ['user' => $user, 'projects' => $projects]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        // dd($id);
        // die();
        $user = Auth::user();
        // echo($user);
        // die();
        $role = Role::all();
        return view('profile', ['user' => $user, 'roles' => $role]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        echo ("en edit");
        die();
        $roles = Role::all();
        return view('role.edit', ['users' => $user], ['roles' => $roles]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateRoles(Request $request)
    {
        $users = User::all();

        foreach ($users as $value) {
            if ($request->id == $value->id) {
                $user = $value;
            }
        }

        $rules = [
            'role_id' => 'required'
        ];

        $request->validate($rules);

        $user->fill($request->all());
        $user->save();

        return Redirect::back()->with('Success', 'Update successful!');
    }
    public function update(Request $request, User $user)
    {
        // $rules = [
        //     'name' => 'required|string|max:255',
        //     'surname' => 'required|max:255',
        //     'email' => 'required',
        //     'role_id' => 'required'
        // ];

        // $request->validate($rules);

        // $user->fill($request->all());
        // $user->save();

        // return Redirect::back()->with('Success', 'Update successful!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
