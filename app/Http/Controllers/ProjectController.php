<?php

namespace App\Http\Controllers;

use App\Models\Project;
use App\Models\Project_users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Redirect;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::all();
        return view('project.project', ['projects' => $projects]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = Project::find($id);
        return view('project.show', ['project' => $project]);
    }

    public function manageUsers()
    {

        //en los detalles del proyecto
        //echo("en manage project users");
    }

    public function manageProjects(Request $request)
    {
        $user = Auth::user();
        $users = User::all();
        $projects = Project::all();
        $roles = Role::all(); //borrar

        $query = User::query();
        $users = $query->paginate(15);


        return view('project.manageProject', ['users' => $users, 'projects' => $projects, 'user' => $user, 'roles' => $roles]);
    }

    public function updateUserProjects(Request $request)
    {

        // dd($request->all());

        $users = User::all();

        //sesgar usuario seleccionado
        foreach ($users as $value) {
            if ($request->id == $value->id) {
                $user = $value;
            }
        }

        // echo($request->project_id);
        // die();

        //recorrer los proyectos para definir si el usuario ya pertenece al proyecto o no para después añadirlo
        foreach($user->project as $project){
            if($request->project_id == $project->id){
                //en caso de encontrado = true
                return Redirect::back()->with('Encontrado', 'The user already belongs to the selected project.');
            }
        }

        $rules = [
            'project_id' => 'required',
            'id' => 'required'
        ];
        $request->validate($rules);

        $prouser = new Project_users;
        $prouser->project_id = $request->project_id;
        $prouser->user_id = $user->id;

        // echo($prouser);
        // die();
        $prouser->save();

        return Redirect::back()->with('Success', 'Update successful!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        return view('project.edit', ['project' => $project]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        //
    }
}
